function topLinePosition() {
    var scrollTop = $(document).scrollTop(),
        headerbottomTop = $(".header-bottom").offset().top;

    if(scrollTop >= headerbottomTop) {
        $(".hb-content").addClass("fixed");
    }
    else {
        $(".hb-content").removeClass("fixed");
    }
}

$(document).ready(function() {

    $('.search_input-box .where_search').styler();

    topLinePosition();
    $(window).scroll(function() {
        topLinePosition();
    });

    $('.bxslider').bxSlider({
        pagerCustom: '#bx-pager',
    });

    $(".lupa").imagezoomsl({
        zoomrange: [1, 12],
        zoomstart: 4,
        innerzoom: true,
        magnifierborder: "none"
    });


    if ($(window).width() <= 1024) {

        $('a.catalog-btn').click(function (e) {

            $('.left_panel').addClass('open');
            $('.overflow').addClass('open');
            $('body').addClass('menu-in');

            e.preventDefault();
        });


        $('i.close-btn, .overflow').click(function () {
            $('.left_panel').removeClass('open');
            $('.overflow').removeClass('open');
            $('body').removeClass('menu-in');
        });


        $('.category-block')
            .addClass('owl-carousel')
            .owlCarousel({
                items: 5,
                nav: true,
                dots: false,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 2
                    },
                    375: {
                        items: 3
                    },
                    641: {
                        items: 4
                    },
                    769: {
                        items: 5
                    }
                }
            });

    }


    $(".city-change select").styler();


    $(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $("#upbutton").addClass("visible");
            checkScrollToTop();
        } else {
            $("#upbutton").removeClass("visible");
        }
    });

    $('#upbutton').click(function(e) {
        e.preventDefault();
        $('html, body').stop().animate({scrollTop : 0}, 800);
    });

    function checkScrollToTop() {
        var e = $(window).scrollTop(),
            t = $(window).height(),
            a = $("footer").offset().top + 30;
        if (a < e + t) {
            $("#upbutton").css("bottom", 55 + e + t - a);
        } else {
            $("#upbutton").css("bottom", 35);
        }
    }


    $('.btn-back').tooltipster({
        side: 'bottom',
        animation: 'fade',
        delay: 200
    });
    $('.input_tooltip').tooltipster({
        side: 'bottom',
        animation: 'fade',
        delay: 200,
        maxWidth: 250
    });
    $('.prop_tooltip').tooltipster({
        side: 'top',
        animation: 'fade',
        delay: 200
    });
    $('.img_tooltip').tooltipster({
        side: 'right',
        animation: 'fade',
        delay: 200,
        contentCloning: true,
        theme: 'tooltipster-previmg'
    });

});