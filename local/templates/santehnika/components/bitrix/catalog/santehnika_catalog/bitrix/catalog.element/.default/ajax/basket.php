<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
use Bitrix\Sale;

$masTovar = json_decode($_POST['mastovar'],true);

$quantity = 1;

$id_tovar = $_REQUEST['tovar_id'];
$SITE_ID = \Bitrix\Main\Context::getCurrent()->getSite();
if($masTovar['SITE_ID'])
{
  $SITE_ID = $masTovar['SITE_ID'];
}

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $SITE_ID);

foreach ($masTovar['value'] as $key => $productId) {
    $i = 0;
    $tugle_id = false;
    $idBasTovar = false;
    foreach ($basket as $basketItem) {

        $idTovar = $basketItem->getProductId();
        $idBasTovar = $basketItem->getId();

        if ($idTovar == $productId){
            $tugle_id = true;
            $bas_product_id = $idBasTovar;
            $in_basket = $i;
        }
        $i++;
    }


    $tugle_id = false;

    if ($tugle_id){

        $basketItems = $basket->getBasketItems();
        $item = $basketItems[$in_basket];

        $item->setField('QUANTITY', $item->getQuantity() + $quantity);

        $basket->save();

    }else{
        $item = $basket->createItem('catalog', $productId);


        $item->setFields([
            'QUANTITY' => $quantity,
            'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
            'LID' => $SITE_ID,
            'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
        ]);

        $basket->save();

    }


    if ($key == 0 && isset($masTovar['servis'])) {
        $basketPropertyCollection = $item->getPropertyCollection();
        if ($masTovar['servis'] == "Y") {
            $basketPropertyCollection->setProperty(array(
                array(
                    'NAME' => GetMessage("TREB_MONTAZH"),
                    'CODE' => 'MONTAZH_YES',
                    'SORT' => 100,
                    'VALUE' => GetMessage("YES"),
                ),
            ));

            $basketPropertyCollection->save();
        } else {
            $basketPropertyCollection->setProperty(array(
                array(
                    'NAME' => GetMessage("TREB_MONTAZH"),
                    'CODE' => 'MONTAZH_YES',
                    'SORT' => 100,
                    'VALUE' => GetMessage("NET"),
                ),
            ));

            $basketPropertyCollection->save();
        }


        if (!empty($masTovar['dop_values'])){
            $basketPropertyCollection = $item->getPropertyCollection();

            $basketPropertyCollection->setProperty(array(
                array(
                    'NAME' =>  GetMessage("DOP_TOVAR_SET"),
                    'CODE' => 'SET_DOP',
                    'SORT' => 100,
                    'VALUE' => implode("|",$masTovar['dop_values']),
                ),
            ));

            $bigTovarId = $item->getId();

            $basketPropertyCollection->save();
        }

    }
    if (is_set($masTovar['dop_values'])) {
        foreach ($masTovar['dop_values'] as $id_dop) {
            if ($productId == $id_dop) {
                $basketPropertyCollection = $item->getPropertyCollection();

                $basketPropertyCollection->setProperty(array(
                    array(
                        'NAME' =>  GetMessage("IS_TOVAR_SET"),
                        'CODE' => 'SET_ID',
                        'SORT' => 100,
                        'VALUE' => $bigTovarId,
                    ),
                ));

                $basketPropertyCollection->save();
            }
        }
    }
}

//CSaleBasket::DeleteAll(1, False);

//?>