$(document).ready(function () {

    // if ($('ul').is('.root-item-menu-in')) {
    //     $('.root-item-menu-in').parent().addClass('menu-in');
    // } else {
    //     $('.root-item-menu-in').parent().addClass('menu-none');
    // };

    var open = $('.open_list-menu');

    open.on('click', function (e) {
        e.preventDefault();
        $(this).closest('.root-item-menu li').find('.root-item-menu-in').slideToggle('300');
        $(this).closest('.root-item-menu li').toggleClass('opened-list_menu-in');
        // $(this).parent().toggleClass('opened-list_menu');
        // $(this).parent().find(".root-item-menu-in").slideToggle(300);
    });
});


var jsvhover = function()
{
	var menuDiv = document.getElementById("catalog-menu");
	if (!menuDiv)
		return;

  var nodes = menuDiv.getElementsByTagName("li");
  for (var i=0; i<nodes.length; i++) 
  {
    nodes[i].onmouseover = function()
    {
      this.className += " jsvhover";
    }
    
    nodes[i].onmouseout = function()
    {
      this.className = this.className.replace(new RegExp(" jsvhover\\b"), "");
    }
  }
}

if (window.attachEvent)
	window.attachEvent("onload", jsvhover);