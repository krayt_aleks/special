<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="news_in_glav">

    <? $i = 1; ?>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        if ($i % 2) {
            $class_one = "right";
            $class_two = "left";
        } else {
            $class_one = "left";
            $class_two = "right";
        }
        ?>
        <div class="news-item clearfix" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="news-item-img <?= $class_one; ?>">
                <div class="img_box">
                    <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                        <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                            <div class="preview_picture" style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>);"></div>
                        <? else: ?>
                            <div class="preview_picture" style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>);"></div>
                        <? endif; ?>
                    <? endif ?>
                </div>
            </div>
            <div class="text_box <?= $class_two; ?>">
                <div class="content">
                    <div class="title_box"><?= $arItem['NAME']; ?></div>
                    <div class="text">
                        <?= $arItem['PREVIEW_TEXT']; ?>
                    </div>
                    <div class="link_box clearfix">
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="detail"><?= GetMessage('PODROBNO'); ?></a>
                        <? if (!empty($arItem['PROPERTIES']['CATALOG']['VALUE'])): ?>
                            <a href="<?= $arItem['PROPERTIES']['CATALOG']['VALUE']['URL'] ?>">
                                <? if (!empty($arItem['PROPERTIES']['TEXT_LINK']['VALUE'])) {
                                    echo $arItem['PROPERTIES']['TEXT_LINK']['VALUE'];
                                } else {
                                    echo $arItem['PROPERTIES']['CATALOG']['VALUE']['NAME'];
                                } ?>
                            </a>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <? $i++; ?>
    <? endforeach; ?>

</section>
