$(document).ready(function () {
    if(!$("html").hasClass('bx-no-touch'))
    {
        return false;
    }
    // var btn = $('.catalog-nav-box');

    // var heightMenu = "420px";
    //
    // btn.hover(
    //     function () {
    //         $(this).addClass('open');
    //         //$('.top_menu_catalog').velocity({'opacity': 1, 'top': 60, 'z-index': 50}, {duration: 50}, [0, 0, 0.58, 1]);
    //         $('.top_menu_catalog').velocity({'opacity': 1, height: heightMenu}, {duration: 50}, [0, 0, 0.58, 1]);
    //         // $('.osn_menu_li[data-select="1"]').addClass('p_select');
    //     },
    //     function () {
    //         $(this).removeClass('open');
    //         //$('.top_menu_catalog').velocity({'opacity': 0, 'top': 55, 'z-index': 0}, {duration: 50}, [0, 0, 0.58, 1]);
    //         $('.top_menu_catalog').velocity({'opacity': 0, height: 0}, {duration: 50}, [0, 0, 0.58, 1]);
    //         // $('.osn_menu_li[data-select="1"]').removeClass('p_select');
    //         // $('.osn_menu_li').removeClass('p_select');
    //     }
    // );
    //

    var btn = $('.catalog-nav-box'),
        menu = $('#top_menu_catalog');

    if ($(window).width() > 1024) {

        btn.hover(
            function () {
                btn.addClass('open');
                intervalID = setTimeout(
                    function () {
                        menu.fadeIn(250);
                    }, 250);
            },
            function () {
                menu.fadeOut('fast');
                clearInterval(intervalID);
                btn.removeClass('open');
            }
        );

    }

    var open = $('.open_top-menu');

    open.on('click', function (e) {
        e.preventDefault();
        $(this).closest('.sub_menu_catalog li').find('.sub_menu_catalog_in').slideToggle('300');
        $(this).closest('.sub_menu_catalog li').toggleClass('opened-list_menu-in');
    });


    // else {
        // btn_link.click(function (e) {
        //     e.preventDefault();
        //     btn_link.toggleClass('active');
        //     menu.toggleClass('menu_open');
        //     $('body').toggleClass('menu-in');
        // })
    // }

    // var block = null;

    // $('.osn_menu_li').hover(
    //     function () {
    //         block = $(this).find('.submenu_box');
    //         block.addClass('edited');
    //
    //         block.velocity({'opacity': 1, 'left': 180, 'z-index': 200}, {duration: 150}, [0, 0, 0.58, 1]);
    //
    //     }, function () {
    //
    //         block = $(this).find('.submenu_box');
    //         block.velocity("stop");
    //
    //         block.velocity({'opacity': 0, 'left': 180, 'z-index': 0}, {duration: 150}, [0, 0, 0.58, 1]);
    //
    //     });
    //
    // $('.osn_menu_li a').hover(function () {
    //     $('.osn_menu_li').removeClass('p_select');
    //     $('.osn_menu_li').removeClass('select');
    //     $(this).parent('.osn_menu_li').addClass('p_select');
    // });


});
