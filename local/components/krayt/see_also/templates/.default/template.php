<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (!empty($arResult)):?>
    <div class="left_podborki_block">
        <div class="title_box">
            <?=GetMessage("TITLE_S");?>
        </div>
        <ul class="brand_block_left">
            <?
			if($arResult)
			foreach ($arResult as $arItem):?>
                    <li>
                        <a href="<?=$arItem['SECTION_PAGE_URL'];?>"><?=$arItem['NAME'];?></a>
                    </li>
            <?endforeach;?>
        </ul>
    </div>
<?endif;?>
