$( document ).ready(function() {

    // $('.collection_element_block .js-carousel').slick({
    //     slidesToShow: 3,
    //     infinite: false,
    //     appendArrows: $(".collection_element_block .product-carousel-nav")
    // });

    // $(".collection_element_block .js-carousel").bxSlider({
    //     minSlides: 1,
    //     maxSlides: 3,
    //     pager: false,
    //     infiniteLoop: false,
    //     slideWidth: 298,
    //     slideMargin: 30,
    //     moveSlides: 1,
    //     responsive: true
    // });

    $(".collection_element_block .js-carousel").owlCarousel({
        items: 3,
        dots: false,
        nav: true,
        margin: 23,
        responsiveClass: true,
        responsive:{
            0:{
                items:1,
                margin: 0
            },
            415:{
                margin: 0,
                items:2
            },
            641: {
                margin: 0,
                items: 3
            },
            1025:{
                items:3
            }
        }
        // afterMove: function (e) {
        //     $('.show_analog').parent().parent().find('.series_arrow_right, .series_arrow_left').removeClass("arrow_disabled");
        //     if (this.owl.currentItem==0) { // стоим в начале
        //         $('.show_analog').parent().parent().find('.series_arrow_left').addClass("arrow_disabled");
        //     }
        //
        //     if (this.currentItem>=this.maximumItem) { // пришли в конец
        //         $('.show_analog').parent().parent().find('.series_arrow_right').addClass("arrow_disabled");
        //     }
        // },
        // afterInit: function(elem) {
        //     if($('.show_analog .owl-item').length > 3)
        //     {
        //         $('.show_analog').parent().parent().find('.series_arrow_right').removeClass("arrow_disabled");
        //         $('.show_analog').parent().parent().find('.series_arrow_left').addClass("arrow_disabled");
        //
        //         $('.show_analog').parent().parent().find('.series_arrow_left a').show();
        //         $('.show_analog').parent().parent().find('.series_arrow_right a').show();
        //     }
        //     else if($('.show_analog .owl-item').length == 1) {
        //         $('.show_analog.owl-carousel .owl-wrapper').css({'width': 'auto'});
        //         $('.show_analog.owl-carousel .owl-item.active').css({'margin': '0 auto', 'float': 'none'});
        //     }
        //     else if($('.show_analog .owl-item').length == 2) {
        //         $('#show_analogs .owl-item').css({'margin-left': '78px'});
        //     }
        // }
    });



});
