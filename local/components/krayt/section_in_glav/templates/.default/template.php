<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!empty($arResult)):?>
    <section class="section_in_glav catalog">
        <div class="title_box"><?=$arParams['TITLE_SECTION'];?></div>
        <div class="category-block">
            <?foreach ($arResult as $arItem):?>
                <a href="<?=$arItem['SECTION_PAGE_URL']?>" class="category-item" title="<?=GetMessage("PEREITI_CAT");?> <?=$arItem['NAME']?>">
                    <?if (empty($arItem['PICTURE'])){
                        $arItem['PICTURE'] = $templateFolder.'/images/no_photo.png';
                    }?>
                    <div class="category-item-img">
                        <div class="img-box">
                            <div class="img" style="background-image: url(<?=$arItem['PICTURE'];?>);"></div>
                        </div>
                    </div>
                    <div class="title"><?=$arItem['NAME']?></div>
                </a>
            <?endforeach;?>
        </div>
        <div class="link-block">
            <a class="btn_anim" href="<?=$arParams['PAGE_SECTION']?>" title="<?=GetMessage("PEREITI_NA");?> <?=GetMessage("VSE_CAT_NA_GLAV");?>">
                <span><?=GetMessage("VSE_CAT_NA_GLAV");?></span>
            </a>
        </div>
    </section>
<?endif;?>
