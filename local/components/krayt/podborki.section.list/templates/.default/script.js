$( document ).ready(function() {

    $(document).on("input",'.podborki_section_serch input[name="podborki_serch"]',function() {

        var value = $(this).val().toUpperCase();
        if (value.length > 2){

            $(".podborki_section_content").each(function () {
                if($(".li_box",$(this)).length == 0)
                {
                    var cat_tex = $(this).text().toUpperCase();
                    if(cat_tex.indexOf(value) + 1) {
                        $(this).show();
                    }else{
                        $(this).hide();
                    }
                }
            });

            $('.li_box').each(function () {
               var link_tex = $(this).text().toUpperCase();
                if(link_tex.indexOf(value) + 1) {
                  //  $(this).show();
                    $(this).show();
                    $(this).parent().parent().show();
                }else{
                    $(this).hide();
                    if($(this).parent().find('.li_box:visible').length == 0)
                    {
                        $(this).parent().parent().hide();
                    }
                }
            });

            if($(".podborki_section_content:visible").length == 0)
            {
                $("#not_cat_str").text(value);
                $("#not_find_category").show();
            }else
            {
                $("#not_find_category").hide();
            }
        }else{
            $(".podborki_section_content").show();
            $('.li_box').show();
        }


    });
});
