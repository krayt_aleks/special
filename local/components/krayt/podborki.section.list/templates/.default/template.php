<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <div class="podborki_section_block">
        <div class="podborki_section_serch">
            <input type="text" name="podborki_serch" placeholder="<?=GetMessage("PODBORCI_SERCH_PLAS");?>">
        </div>
                <?foreach ($arResult as $section):?>
                    <div class="podborki_section_content">
                        <h2><a class="link_podborki big shine" style="background-image: url('<?=$section['DETAIL_PICTURE_SRC']?>');" href='<?=$section['SECTION_PAGE_URL'];?><?=$section['UF_PODBORKI_FILTER'];?>' title="<?=GetMessage("PODBORCI_SERCH_IN_RAZDEL");?> <?=$section['NAME'];?>"><?=$section['NAME'];?><span class="figure"></span></a></h2>
                        <?Podborci($section);?>
                    </div>
                <?endforeach;?>
        </div>
        <div id="not_find_category">
            <?=GetMessage('PODBORCI_SERCH_NOT_CATEG')?>
        </div>
    </div>
<?endif;?>





