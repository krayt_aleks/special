<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "+7 987 654-32-10, +7 800 976-54-32, Ежедневно с 9.00 до 20.00");
$APPLICATION->SetPageProperty("keywords", "Контакты");
$APPLICATION->SetPageProperty("title", "Контакты");
$APPLICATION->SetTitle("Контакты");
?><div class="contacts">
	<h1>Контакты</h1>
	<div class="row">
		<div class="col-md-5 col-sm-4 col-xs-12">
			<div class="contact">
				<div class="item">
					<h4>Телефон</h4>
					<p>
 <b>+7 987 654-32-10</b><br>
 <b>
						+7 800 976-54-32</b><br>
						 Ежедневно с 9.00 до 20.00
					</p>
				</div>
				<div class="item">
					<h4>E-mail</h4>
					<p>
 <a href="mailto:info@e-market.ru"><b>info@e-market.ru</b></a><br>
						 Для информации о сотрудничестве, коммерческих предложениях
					</p>
				</div>
				<div class="item">
					<h4>Адрес</h4>
					<p>
						 141031, Россия, МО, Мытищи, Осташковское шоссе,<br>
						 владение 5, строение 1.
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-7 col-sm-8 col-xs-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array(
			0 => "ZOOM",
			1 => "TYPECONTROL",
		),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:5:{s:10:\"yandex_lat\";d:55.71499513960177;s:10:\"yandex_lon\";d:37.79372001748897;s:12:\"yandex_scale\";i:15;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.79269797397484;s:3:\"LAT\";d:55.712127516733055;s:4:\"TEXT\";s:0:\"\";}}s:9:\"POLYLINES\";a:1:{i:0;a:3:{s:6:\"POINTS\";a:5:{i:0;a:2:{s:3:\"LAT\";d:55.717237800395;s:3:\"LON\";d:37.793682344685;}i:1;a:2:{s:3:\"LAT\";d:55.716511045485;s:3:\"LON\";d:37.797244318257;}i:2;a:2:{s:3:\"LAT\";d:55.712562106467;s:3:\"LON\";d:37.796042688618;}i:3;a:2:{s:3:\"LAT\";d:55.713337389926;s:3:\"LON\";d:37.793682344685;}i:4;a:2:{s:3:\"LAT\";d:55.712126002731;s:3:\"LON\";d:37.792695291768;}}s:5:\"TITLE\";s:30:\"Маршрут от метро\";s:5:\"STYLE\";a:2:{s:11:\"strokeColor\";s:8:\"0072BC7F\";s:11:\"strokeWidth\";i:5;}}}}",
		"MAP_HEIGHT" => "450",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(
			0 => "ENABLE_DBLCLICK_ZOOM",
			1 => "ENABLE_DRAGGING",
		)
	),
	false
);?>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>