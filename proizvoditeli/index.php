<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Производители");
?><?$APPLICATION->IncludeComponent(
	"krayt:brend", 
	".default", 
	array(
		"CACHE_TIME" => "360000",
		"CACHE_TYPE" => "A",
		"IBLOCK_TYPE" => "catalog",
		"I_BLOCK" => "10",
		"SEF_FOLDER" => "/proizvoditeli/",
		"SEF_MODE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"PAGER_SHOW_ALWAYS_TIP" => "Y",
		"ELEMENT_COL" => "18",
		"IBLOCK_TYPE_CATALOG" => "catalog",
		"I_BLOCK_CATALOG" => "2",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROP_1" => "STRANA",
		"PROP_2" => "OBEM",
		"PROP_3" => "BISTROSEMNOE_SIDENIE",
		"PROP_4" => "BREND",
		"PROP_5" => "BEZOBODKOVIY",
		"LABEL_PROP" => array(
		),
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"DISCOUNT_PERCENT_POSITION" => "top-left",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE_PATH#/",
			"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#",
		)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>